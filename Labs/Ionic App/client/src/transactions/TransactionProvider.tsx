import React, {useCallback, useContext, useEffect, useReducer, useState} from 'react';
import PropTypes from 'prop-types';
import {getLogger} from '../core';
import {TransactionProperties} from './TransactionProperties';
import {createTransactionApi, getTransactionsApi, newWebSocket, syncTransactions, updateTransactionApi} from './transactionApi';
import {AuthContext} from '../auth';
import {Plugins} from '@capacitor/core';

const log = getLogger('TransactionProvider');

export type SaveTransactionFn = (transaction: TransactionProperties) => Promise<any>;
const {Storage} = Plugins

export interface TransactionsState {
    transactions?: TransactionProperties[],
    fetching: boolean,
    fetchingError?: Error | null,
    saving: boolean,
    savingError?: Error | null,
    saveTransaction?: SaveTransactionFn,
    connectedNetwork?: boolean,
    setSavedOffline?: Function,
    savedOffline?: boolean
}

interface ActionProps {
    type: string,
    payload?: any,
}

const initialState: TransactionsState = {
    fetching: false,
    saving: false,
};

const FETCH_TRANSACTIONS_STARTED = 'FETCH_TRANSACTIONS_STARTED';
const FETCH_TRANSACTIONS_SUCCEEDED = 'FETCH_TRANSACTIONS_SUCCEEDED';
const FETCH_TRANSACTIONS_FAILED = 'FETCH_TRANSACTIONS_FAILED';
const SAVE_TRANSACTION_STARTED = 'SAVE_TRANSACTION_STARTED';
const SAVE_TRANSACTION_SUCCEEDED = 'SAVE_TRANSACTION_SUCCEEDED';
const SAVE_TRANSACTION_FAILED = 'SAVE_TRANSACTION_FAILED';

const reducer: (state: TransactionsState, action: ActionProps) => TransactionsState =
    (state, {type, payload}) => {
        switch (type) {
            case FETCH_TRANSACTIONS_STARTED:
                return {...state, fetching: true, fetchingError: null};
            case FETCH_TRANSACTIONS_SUCCEEDED:
                return {...state, transactions: payload.transactions, fetching: false};
            case FETCH_TRANSACTIONS_FAILED:
                return {...state, fetchingError: payload.error, fetching: false};
            case SAVE_TRANSACTION_STARTED:
                return {...state, savingError: null, saving: true};
            case SAVE_TRANSACTION_SUCCEEDED:
                const transactions = [...(state.transactions || [])];
                const transaction = payload.transaction;
                const index = transactions.findIndex(it => it._id === transaction._id);
                if (index === -1) {
                    transactions.splice(0, 0, transaction);
                } else {
                    transactions[index] = transaction;
                }
                return {...state, transactions: transactions, saving: false};
            case SAVE_TRANSACTION_FAILED:
                return {...state, savingError: payload.error, saving: false};
            default:
                return state;
        }
    };

export const TransactionContext = React.createContext<TransactionsState>(initialState);

interface TransactionProviderProps {
    children: PropTypes.ReactNodeLike,
}

const {Network} = Plugins

export const TransactionProvider: React.FC<TransactionProviderProps> = ({children}) => {
    const {token} = useContext(AuthContext);

    const [connectedNetworkStatus, setConnectedNetworkStatus] = useState<boolean>(false);
    Network.getStatus().then(status => setConnectedNetworkStatus(status.connected));
    const [savedOffline, setSavedOffline] = useState<boolean>(false);
    useEffect(networkEffect, [token, setConnectedNetworkStatus]);

    const [state, dispatch] = useReducer(reducer, initialState);
    const {transactions, fetching, fetchingError, saving, savingError} = state;
    useEffect(getTransactionsEffect, [token]);
    useEffect(ws, [token]);
    const saveTransaction = useCallback<SaveTransactionFn>(saveTransactionCallback, [token]);
    const value = {
        transactions, fetching, fetchingError, saving, savingError, saveTransaction, connectedNetworkStatus,
        savedOffline,
        setSavedOffline
    };
    log('returns');
    return (
        <TransactionContext.Provider value={value}>
            {children}
        </TransactionContext.Provider>
    );

    function networkEffect() {
        console.log("network effect");
        let canceled = false;
        Network.addListener('networkStatusChange', async (status) => {
            if (canceled) return;
            const connected = status.connected;
            if (connected) {
                console.log("networkEffect - SYNC data");
                await syncTransactions(token);
            }
            setConnectedNetworkStatus(status.connected);
        });
        return () => {
            canceled = true;
        }
    }

    function getTransactionsEffect() {
        let canceled = false;
        fetchTransactions();
        return () => {
            canceled = true;
        }

        async function fetchTransactions() {
            let canceled = false;
            fetchTransactions();
            return () => {
                canceled = true;
            }

            async function fetchTransactions() {
                if (!token?.trim()) return;
                if (!navigator?.onLine) {
                    let storageKeys = Storage.keys();
                    const transactions = await storageKeys.then(async function (storageKeys) {
                        const saved = [];
                        for (let i = 0; i < storageKeys.keys.length; i++) {
                            if (storageKeys.keys[i] !== "token") {
                                const transaction = await Storage.get({key: storageKeys.keys[i]});
                                if (transaction.value != null)
                                    var parsedTransaction = JSON.parse(transaction.value);
                                saved.push(parsedTransaction);
                            }
                        }
                        return saved;
                    });
                    dispatch({type: FETCH_TRANSACTIONS_SUCCEEDED, payload: {transactions: transactions}});
                } else {
                    try {
                        log('fetchTransactions started');
                        dispatch({type: FETCH_TRANSACTIONS_STARTED});
                        const transactions = await getTransactionsApi(token);
                        log('fetchTransactions successful');
                        if (!canceled) {
                            dispatch({type: FETCH_TRANSACTIONS_SUCCEEDED, payload: {transactions: transactions}})
                        }
                    } catch (error) {
                        let storageKeys = Storage.keys();
                        const transactions = await storageKeys.then(async function (storageKeys) {
                            const saved = [];
                            for (let i = 0; i < storageKeys.keys.length; i++) {
                                if (storageKeys.keys[i] !== "token") {
                                    const transaction = await Storage.get({key: storageKeys.keys[i]});
                                    if (transaction.value != null)
                                        var parsedTransaction = JSON.parse(transaction.value);
                                    saved.push(parsedTransaction);
                                }
                            }
                            return saved;
                        });
                        dispatch({type: FETCH_TRANSACTIONS_SUCCEEDED, payload: {transactions: transactions}});
                    }
                }

            }
        }
    }


    async function saveTransactionCallback(transaction: TransactionProperties) {
        try {
            if (navigator.onLine) {
                log('saveTransaction started');
                dispatch({type: SAVE_TRANSACTION_STARTED});
                const updatedTransaction = await (transaction._id ? updateTransactionApi(token, transaction) : createTransactionApi(token, transaction))
                log('saveTransaction successful');
                dispatch({type: SAVE_TRANSACTION_SUCCEEDED, payload: {transaction: updatedTransaction}});
            } else {
                log('saveTransaction failed');
                transaction._id = (transaction._id == undefined) ? ('_' + Math.random().toString(36).substr(2, 9)) : transaction._id;
                await Storage.set({
                    key: transaction._id!,
                    value: JSON.stringify({
                        _id: transaction._id,
                        type: transaction.type,
                        amount: transaction.amount,
                        date: transaction.date,
                        madeByPartner: transaction.madeByPartner,
                        category: transaction.category
                    })
                });
                alert('Transaction saved locally. Get back online to synchronize to server');
                dispatch({type: SAVE_TRANSACTION_SUCCEEDED, payload: {transaction: transaction}});
                setSavedOffline(true);
            }
        } catch (error) {
            log('saveTransaction failed');
            await Storage.set({
                key: String(transaction._id),
                value: JSON.stringify(transaction)
            })
            dispatch({type: SAVE_TRANSACTION_SUCCEEDED, payload: {transaction: transaction}});
        }
    }

    function ws() {
        let canceled = false;
        log('wsEffect - connecting');
        let closeWebSocket: () => void;
        if (token?.trim()) {
            closeWebSocket = newWebSocket(token, message => {
                if (canceled) {
                    return;
                }
                const {type, payload: transaction} = message;
                log(`ws message, transaction ${type}`);
                if (type === 'created' || type === 'updated') {
                    dispatch({type: SAVE_TRANSACTION_SUCCEEDED, payload: {transaction}});
                }
            });
        }
        return () => {
            log('wsEffect - disconnecting');
            canceled = true;
            closeWebSocket?.();
        }
    }
}
