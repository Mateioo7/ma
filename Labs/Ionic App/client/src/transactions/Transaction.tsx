import React from 'react';
import {IonItem, IonLabel} from '@ionic/react';
import {TransactionProperties} from './TransactionProperties';
import {getAmountColor} from "./transactionUtils";

interface TransactionPropertiesExtended extends TransactionProperties {
    onEdit: (id?: string) => void;
}

const Transaction: React.FC<TransactionPropertiesExtended> = (
    {_id, type, amount, category, date, onEdit}) => {
    return (
        <IonItem onClick={() => onEdit(_id)}>
            <IonLabel>{category}</IonLabel>
            <IonLabel color={getAmountColor(type)}>{amount} RON</IonLabel>
            <IonLabel>{date}</IonLabel>
        </IonItem>
    );
};

export default Transaction;
