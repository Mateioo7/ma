import {GeolocationPosition} from "@capacitor/core";

export interface TransactionProperties {
    _id?: string
    type: string
    amount: number
    date: string
    madeByPartner: boolean
    category: string
    image: string
    position: GeolocationPosition
}
