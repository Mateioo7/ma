import axios from 'axios';
import {authConfig, getLogger} from '../core';
import {TransactionProperties} from './TransactionProperties';
import {Plugins} from "@capacitor/core";

const log = getLogger('itemApi');
const {Storage} = Plugins;

const baseUrl = 'localhost:3000';
const transactionUrl = `http://${baseUrl}/api/transactions`;

interface ResponseProps<T> {
    data: T;
}

function withLogs<T>(promise: Promise<ResponseProps<T>>, fnName: string): Promise<T> {
    log(`${fnName} - started`);
    return promise
        .then(res => {
            log(`${fnName} - succeeded`);
            return Promise.resolve(res.data);
        })
        .catch(err => {
            log(`${fnName} - failed`);
            return Promise.reject(err);
        });
}

const config = {
    headers: {
        'Content-Type': 'application/json'
    }
};

export const getTransactionsApi: (token: string) => Promise<TransactionProperties[]> = async (token) => {
    return withLogs(axios.get(transactionUrl, authConfig(token)), 'getTransactionsApi');
}

export const createTransactionApi: (token: string, transaction: TransactionProperties) => Promise<TransactionProperties[]> = async (token, transaction) => {
    return withLogs(axios.post(transactionUrl, transaction, authConfig(token)), 'createTransactionApi');
}

export const updateTransactionApi: (token: string, transaction: TransactionProperties) => Promise<TransactionProperties[]> = (token, transaction) => {
    return withLogs(axios.put(`${transactionUrl}/${transaction._id}`, transaction, authConfig(token)), 'updateTransactionApi');
}

export const isDifferent = (transaction1: TransactionProperties, transaction2: TransactionProperties) => {
    return !(transaction1.type === transaction2.type
        && transaction1.amount === transaction2.amount
        && transaction1.date === transaction2.date
        && transaction1.madeByPartner === transaction2.madeByPartner
        && transaction1.category === transaction2.category);
}

export const syncTransactions: (token: string) => Promise<TransactionProperties[]> = async token => {
    try {
        const {keys} = await Storage.keys();
        const result: any = axios.get(`${transactionUrl}/`, authConfig(token));
        result.then(async (result: { data: any[]; }) => {
            for (const i of keys) {
                if (i !== 'token') {
                    const serverTransaction: TransactionProperties = result.data.find((each: { _id: string; }) => each._id === i);
                    const localTransaction = await Storage.get({key: i});

                    if (serverTransaction !== undefined && isDifferent(serverTransaction, JSON.parse(localTransaction.value!))) {
                        // sync transactions which were updated offline
                        console.log('UPDATE ' + localTransaction.value);
                        await axios.put(`${transactionUrl}/${i}`, JSON.parse(localTransaction.value!), authConfig(token));
                    } else if (serverTransaction === undefined) {
                        // create transactions which were saved offline
                        console.log('CREATE' + localTransaction.value!);
                        await axios.post(`${transactionUrl}`, JSON.parse(localTransaction.value!), authConfig(token));
                    }
                }
            }
        }).catch((err: { response: any; request: any; }) => {
            if (err.response) {
                console.log('client received an error response (5xx, 4xx)');
            } else if (err.request) {
                console.log('client never received a response, or request never left');
            } else {
                console.log('anything else');
            }
        })
        return withLogs(result, 'syncTransactions');
    } catch (error) {
        throw error;
    }
}

interface MessageData {
    type: string;
    payload: {
        transaction: TransactionProperties;
    };
}

export const newWebSocket = (token: string, onMessage: (data: MessageData) => void) => {
    const ws = new WebSocket(`ws://${baseUrl}`);
    ws.onopen = () => {
        log('web socket onopen');
        ws.send(JSON.stringify({type: 'authorization', payload: {token}}));
    };
    ws.onclose = () => {
        log('web socket onclose');
    };
    ws.onerror = error => {
        log('web socket onerror', error);
    };
    ws.onmessage = messageEvent => {
        log('web socket onmessage');
        onMessage(JSON.parse(messageEvent.data));
    };
    return () => {
        ws.close();
    }
}

