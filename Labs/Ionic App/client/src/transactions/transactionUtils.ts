import dayjs from "dayjs";
import advancedFormat from "dayjs/plugin/advancedFormat";

export function getAmountColor(type: string) {
    if (type == 'income') {
        return "#66DE93";
    } else if (type == "expense") {
        return "#C84B31";
    }
}

export const ionDateTimeFormat = "MMM DD, YYYY";

export const prettyDateFormat = 'MMMM Do, YYYY'

export function getPrettyDate(date: Date) {
    dayjs(date);
    dayjs.extend(advancedFormat)
    return dayjs().format(prettyDateFormat).toString();
}

export function stringToDate(date: string): string {
    // console.log("dateee: " + date)
    return String(date);
}

export function dateWithoutTimezone(date: string | null | undefined): string {
    date = String(date)
    // alert(date)
    return date.split("+")[0];

    // return date.split()
    // return date.toLocaleString().split('T')[0];
}

