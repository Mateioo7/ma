import React, {useContext, useEffect, useState} from 'react';
import {
    IonButton,
    IonButtons,
    IonCheckbox,
    IonContent,
    IonDatetime, IonFabButton,
    IonHeader, IonIcon, IonImg,
    IonInput,
    IonItem,
    IonLabel,
    IonLoading,
    IonPage,
    IonSelect,
    IonSelectOption,
    IonTitle,
    IonToolbar
} from '@ionic/react';
import {getLogger} from '../core';
import {TransactionContext} from './TransactionProvider';
import {RouteComponentProps} from 'react-router';
import {TransactionProperties} from './TransactionProperties';
import {dateWithoutTimezone, ionDateTimeFormat} from "./transactionUtils";
import { useCamera } from "@ionic/react-hooks/camera";
import {Camera, CameraResultType, CameraSource, Geolocation, GeolocationPosition} from "@capacitor/core";
import { camera } from 'ionicons/icons';
import {usePhotoGallery} from "../core/usePhotoGallery";
import {MyMap} from "../core/MyMap";

const log = getLogger('TransactionEdit');

interface ItemEditProps extends RouteComponentProps<{
    id?: string;
}> {
}

type Await<T> = T extends {
    then(onfulfilled?: (value: infer U) => unknown): unknown;
} ? U : T;


const TransactionEdit: React.FC<ItemEditProps> = ({history, match}) => {
    const {transactions, saving, savingError, saveTransaction} = useContext(TransactionContext);

    const [type, setType] = useState('');
    const [amount, setAmount] = useState(0);
    const [date, setDate] = useState('');
    const [madeByPartner, setMadeByPartner] = useState(false);
    const [category, setCategory] = useState('');

    const [image, setImage] = useState('');
    // const { photos, takePhoto, deletePhoto } = usePhotoGallery();
    const { getPhoto } = useCamera();

    const [position, setPosition] = useState<GeolocationPosition>()

    // console.log('partner: ', madeByPartner);
    // console.log('category: ', category);

    const [item, setItem] = useState<TransactionProperties>();

    useEffect(() => {
        log('useEffect');
        const routeId = match.params.id || '';
        console.table(transactions)
        const item = transactions?.find(it => it._id == routeId);
        setItem(item);
        console.table(item)

        if (item) {
            setType(item.type);
            setAmount(item.amount);
            setDate(item.date)
            setMadeByPartner(item.madeByPartner);
            setCategory(item.category);
            setImage(item.image);
            setPosition(item.position);
        }
    }, [match.params.id, transactions]);

    const handleSave = () => {
        const editedItem = item ? {
            ...item,
            type: type,
            amount: amount,
            date: date,
            madeByPartner: madeByPartner,
            category: category,
            image: image,
            position: position
        } : {type, amount, date, madeByPartner, category, image, position};
        saveTransaction && saveTransaction(editedItem as TransactionProperties).then(() => history.goBack());
    };

    const takePhoto = async () => {
        const cameraPhoto = await getPhoto({
            resultType: CameraResultType.Base64,
            source: CameraSource.Camera,
            quality: 100
        });
        setImage(cameraPhoto.base64String ?? "")
    };

    log('render');

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Edit</IonTitle>
                    <IonButtons slot="end">
                        <IonButton onClick={handleSave}>
                            Save
                        </IonButton>
                    </IonButtons>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonItem>
                    <IonLabel position="floating">Type of transaction</IonLabel>
                    <IonSelect value={type} onIonChange={e => setType(e.detail.value)}>
                        <IonSelectOption value="income">Income</IonSelectOption>
                        <IonSelectOption value="expense">Expense</IonSelectOption>
                        <IonSelectOption value="transfer">Transfer</IonSelectOption>
                    </IonSelect>
                </IonItem>

                <IonItem>
                    <IonLabel position="floating">Amount</IonLabel>
                    <IonInput type="number" value={amount}
                              onIonChange={e => setAmount(parseInt(e.detail.value!, 10) || 0)}/>
                </IonItem>

                <IonItem>
                    <IonLabel position="floating">Category</IonLabel>
                    <IonInput value={category} onIonChange={e => setCategory(e.detail.value || '')}/>
                </IonItem>

                <IonItem>
                    <IonLabel position="floating">Date</IonLabel>
                    <IonDatetime displayFormat={ionDateTimeFormat} value={date}
                                 onIonChange={e => setDate(dateWithoutTimezone(e.detail.value))}/>
                </IonItem>

                <IonItem>
                    <IonLabel>Added by partner</IonLabel>
                    <IonCheckbox checked={madeByPartner} onIonChange={e => setMadeByPartner(e.detail.checked)}/>
                </IonItem>


                <IonItem>
                    <IonLabel>Take a photo</IonLabel>
                    <IonImg src={`data:image/png;base64,${image}`} draggable="false" />
                    {/*<ByteImage byteSrc={car.image} />*/}
                    <IonFabButton onClick={() => takePhoto()}>
                        <IonIcon icon={camera}/>
                    </IonFabButton>


                    {/*<IonButton*/}
                    {/*    color="secondary"*/}
                    {/*    onClick={takePhoto}>*/}
                    {/*    Take a picture*/}
                    {/*</IonButton>*/}
                </IonItem>

                {/*<IonItem>*/}
                    <MyMap
                        lat={position ? (position.coords ? position.coords.latitude : 0.0) : 0.0}
                        lng={position ?  (position.coords ? position.coords.longitude : 0.0)  : 0.0}
                        onMapClick={(e: any) => {
                            console.log(e.latLng.lat(), e.latLng.lng())
                            setPosition({
                                coords: {
                                    latitude: e.latLng.lat(),
                                    longitude: e.latLng.lng(),
                                    accuracy: e.latLng.accuracy,
                                },
                                timestamp: Date.now()
                            })
                        }}
                        onMarkerClick={log('onMarker')}
                    />
                {/*</IonItem>*/}

                <IonLoading isOpen={saving}/>
                {savingError && (
                    <div>{savingError.message || 'Failed to save transaction'}</div>
                )}
            </IonContent>
        </IonPage>
    );
};

export default TransactionEdit;
