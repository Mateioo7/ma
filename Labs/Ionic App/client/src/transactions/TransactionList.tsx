import React, {useContext, useEffect, useState} from 'react';
import {RouteComponentProps} from 'react-router';
import {
    IonChip,
    IonContent,
    IonFab,
    IonFabButton,
    IonHeader,
    IonIcon,
    IonInfiniteScroll,
    IonInfiniteScrollContent,
    IonLabel,
    IonList,
    IonLoading,
    IonPage,
    IonToast,
    IonToolbar
} from '@ionic/react';
import {add, logOut} from 'ionicons/icons';
import {getLogger} from '../core';
import {TransactionContext} from './TransactionProvider';
import {Network} from '@capacitor/core';
import {AuthContext} from '../auth';
import Transaction from "./Transaction";

const log = getLogger('TransactionList');
const offset = 3;

const TransactionList: React.FC<RouteComponentProps> = ({history}) => {
    const {logout} = useContext(AuthContext)
    const {transactions, fetching, fetchingError} = useContext(TransactionContext);
    const [disableInfiniteScroll, setDisabledInfiniteScroll] = useState<boolean>(false);
    const [page, setPage] = useState(offset)
    const [status, setStatus] = useState<boolean>(true);

    const {savedOffline, setSavedOffline} = useContext(TransactionContext);

    Network.getStatus().then(status => setStatus(status.connected));
    Network.addListener('networkStatusChange', (status) => {
        setStatus(status.connected);
    });

    useEffect(() => {
        if (transactions?.length && transactions?.length > 0) {
            setPage(offset);
            fetchData();
            console.log(transactions);
        }
    }, [transactions]);


    function fetchData() {
        setPage(page + offset);
        if (transactions && page > transactions?.length) {
            setDisabledInfiniteScroll(true);
            setPage(transactions.length);
        } else {
            setDisabledInfiniteScroll(false);
        }
    }

    async function searchNext($event: CustomEvent<void>) {
        fetchData();
        ($event.target as HTMLIonInfiniteScrollElement).complete();
    }

    log('render');
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonChip>
                        <IonLabel color={status ? "success" : "danger"}>{status ? "Online" : "Offline"}</IonLabel>
                    </IonChip>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonLoading isOpen={fetching} message="Fetching transactions"/>
                {transactions && (
                    <IonList>
                        {transactions.map(({_id, type, amount, date, madeByPartner, category, image, position}) =>
                            <Transaction key={_id} _id={_id} type={type} amount={amount} date={date}
                                         madeByPartner={madeByPartner} category={category} image={image} position={position}
                                         onEdit={id => history.push(`/transactions/${id}`)}/>)}
                    </IonList>
                )}
                {fetchingError && (
                    <div>{fetchingError.message || 'Failed to fetch transactions'}</div>
                )}
                <IonInfiniteScroll threshold="100px" disabled={disableInfiniteScroll}
                                   onIonInfinite={(e: CustomEvent<void>) => searchNext(e)}>
                    <IonInfiniteScrollContent loadingText="Loading...">
                    </IonInfiniteScrollContent>
                </IonInfiniteScroll>

                {
                    fetchingError && (
                        <div>{fetchingError.message || 'Failed to fetch transactions'}</div>
                    )
                }

                <IonFab vertical="bottom" horizontal="end" slot="fixed">
                    <IonFabButton onClick={() => history.push('/transactions/add')}>
                        <IonIcon icon={add}/>
                    </IonFabButton>
                    <IonFabButton onClick={() => handleLogout()} href='/login'>
                        <IonIcon icon={logOut}/>
                    </IonFabButton>
                </IonFab>

                <IonToast
                    isOpen={!!savedOffline}
                    message="Your changes will be visible on server when you get back online!"
                    duration={2000}/>
            </IonContent>
        </IonPage>
    );

    function handleLogout() {
        log("logout");
        logout?.();
    }
};

export default TransactionList;
