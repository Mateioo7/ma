import React from "react";
import {GoogleMap, Marker, withGoogleMap, withScriptjs} from 'react-google-maps';
import {compose, withProps} from 'recompose';
import {apiKey} from "./index";

interface MyMapProps {
    lat?: number;
    lng?: number;
    onMapClick: (e: any) => void,
    onMarkerClick: (e: any) => void,
}


export const MyMap =
    compose<MyMapProps, any>(
        withProps({
            googleMapURL:
                `https://maps.googleapis.com/maps/api/js?key=${apiKey}&v=3.exp&libraries=geometry,drawing,places`,
            loadingElement: <div style={{height: `100%`}}/>,
            containerElement: <div style={{height: `400px`}}/>,
            mapElement: <div style={{height: `100%`}}/>
        }),
        withScriptjs,
        withGoogleMap
    )(props => (
        // @ts-ignore
        <GoogleMap
            defaultZoom={8}
            defaultCenter={{lat: props.lat, lng: props.lng}}
            onClick={props.onMapClick}>
            // @ts-ignore
            <Marker
                position={{lat: props.lat, lng: props.lng}}
                onClick={props.onMarkerClick}
            />
        </GoogleMap>
    ))
