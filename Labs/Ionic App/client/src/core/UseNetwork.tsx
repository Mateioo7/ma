import {useEffect, useState} from "react";
import {NetworkStatus, Plugins} from "@capacitor/core";
import {getLogger} from "./index";

const {Network} = Plugins;
const logger = getLogger('UseNetwork');

const initialState = {
    connected: false,
    connectionType: 'unknown',
}

export const useNetwork = () => {
    const [networkStatus, setNetworkStatus] = useState(initialState);

    useEffect(() => {
        // each time the status of the network is changed, this handler will call handleNetworkStatusChange function
        // to update it
        const handler = Network.addListener('networkStatusChange', handleNetworkStatusChange);

        // when the object which uses this custom hook is rendered and mounted, this function is called in order to set
        // the actual status of the network
        Network.getStatus().then(handleNetworkStatusChange);

        let canceled = false;
        // this return section is called when the object which uses this custom hook is unmounted
        return () => {
            // when the object which uses this custom hook is unmounted, the handler is removed
            canceled = true;
            handler.remove();
        }

        function handleNetworkStatusChange(status: NetworkStatus) {
            logger('useNetwork - status change: ' + status);
            if (!canceled) {
                setNetworkStatus(status);
            }
        }
    }, []);

    return {networkStatus};
}