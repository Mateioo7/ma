import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {IonApp, IonRouterOutlet} from '@ionic/react';
import {IonReactRouter} from '@ionic/react-router';
import {TransactionEdit, TransactionList} from './transactions';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import {TransactionProvider} from './transactions/TransactionProvider';
import {AuthProvider, Login, PrivateRoute} from "./auth";

const App: React.FC = () => (
    <IonApp>
        <IonReactRouter>
            <IonRouterOutlet>
                <AuthProvider>
                    <Route path="/login" component={Login} exact={true}/>
                    <Route exact path="/" render={() => <Redirect to="/transactions"/>}/>
                    <TransactionProvider>
                        <PrivateRoute path="/transactions" component={TransactionList} exact={true}/>
                        <PrivateRoute path="/transactions/:id" component={TransactionEdit} exact={true}/>
                        <PrivateRoute path="/transactions/add" component={TransactionEdit} exact={true}/>
                    </TransactionProvider>
                </AuthProvider>
            </IonRouterOutlet>
        </IonReactRouter>


        {/*<TransactionProvider>*/}
        {/*    <IonReactRouter>*/}
        {/*        <IonRouterOutlet>*/}
        {/*            <PrivateRoute path="/login" component={Login} exact={true}/>*/}
        {/*            <PrivateRoute path="/transactions" component={ItemList} exact={true}/>*/}
        {/*            <PrivateRoute path="/transactions/:id" component={ItemEdit} exact={true}/>*/}
        {/*        </IonRouterOutlet>*/}
        {/*        <PrivateRoute exact path="/" render={() => <Redirect to="/transactions"/>}/>*/}

        {/*    </IonReactRouter>*/}
        {/*</TransactionProvider>*/}
    </IonApp>
);

export default App;
