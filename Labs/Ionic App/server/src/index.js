import Koa from 'koa';
import WebSocket from 'ws';
import http from 'http';
import Router from 'koa-router';
import bodyParser from "koa-bodyparser";
import jwt from 'koa-jwt';
import cors from '@koa/cors';

import {exceptionHandler, initWss, jwtConfig, timingLogger} from './utils';
import {router as transactionRouter} from './transaction/router';
import {router as userRouter} from './user/router';

const application = new Koa();
const server = http.createServer(application.callback());
const webSocketServer = new WebSocket.Server({server});

initWss(webSocketServer);

application.use(cors());
application.use(timingLogger);
application.use(exceptionHandler);
application.use(bodyParser());

const prefix = '/api';

// public routes
const publicApiRouter = new Router({prefix});
publicApiRouter.use('/auth', userRouter.routes());
application.use(publicApiRouter.routes())
    .use(publicApiRouter.allowedMethods());

application.use(jwt(jwtConfig));

// protected routes
const protectedApiRouter = new Router({prefix});
protectedApiRouter.use('/transactions', transactionRouter.routes());
application.use(protectedApiRouter.routes())
    .use(protectedApiRouter.allowedMethods());

server.listen(3000);
console.log('started on port 3000');
