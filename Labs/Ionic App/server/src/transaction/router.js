import Router from 'koa-router';
import transactionStore from './store';
import {broadcast} from "../utils";

export const router = new Router();

router.get('/', async (ctx) => {
    const response = ctx.response;
    const userId = ctx.state.user._id;
    response.body = await transactionStore.find({userId});
    response.status = 200; // ok
});

router.get('/:id', async (ctx) => {
    const userId = ctx.state.user._id;
    const transaction = await transactionStore.findOne({_id: ctx.params.id});
    const response = ctx.response;
    if (transaction) {
        if (transaction.userId === userId) {
            response.body = transaction;
            response.status = 200; // ok
        } else {
            response.status = 403; // forbidden
        }
    } else {
        response.status = 404; // not found
    }
});

const createTransaction = async (ctx, transaction, response) => {
    try {
        const userId = ctx.state.user._id;
        transaction.userId = userId;
        response.body = await transactionStore.insert(transaction);
        response.status = 201; // created
        broadcast(userId, {type: 'created', payload: transaction});
    } catch (err) {
        response.body = {message: err.message};
        response.status = 400; // bad request
    }
};

router.post('/', async ctx => await createTransaction(ctx, ctx.request.body, ctx.response));

router.put('/:id', async (ctx) => {
    const transaction = ctx.request.body;
    const id = ctx.params.id;
    const transactionId = transaction._id;
    const response = ctx.response;
    if (transactionId && transactionId !== id) {
        response.body = {message: 'Param id and body _id should be the same'};
        response.status = 400; // bad request
        return;
    }
    if (!transactionId) {
        await createTransaction(ctx, transaction, response);
    } else {
        const userId = ctx.state.user._id;
        transaction.userId = userId;
        const updatedCount = await transactionStore.update({_id: id}, transaction);
        if (updatedCount === 1) {
            response.body = transaction;
            response.status = 200; // ok
            broadcast(userId, {type: 'updated', payload: transaction});
        } else {
            response.body = {message: 'Resource no longer exists'};
            response.status = 405; // method not allowed
        }
    }
});
