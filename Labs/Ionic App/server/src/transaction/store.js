import dataStore from 'nedb-promise';

export class TransactionStore {
    constructor({filename, autoload}) {
        this.store = dataStore({filename, autoload});
    }

    async find(props) {
        return this.store.find(props);
    }

    async findOne(props) {
        return this.store.findOne(props);
    }

    async insert(transaction) {
        if (!transaction.type) {
            throw new Error('The type of the transaction is missing');
        }
        if (!transaction.amount) {
            throw new Error('The amount of the transaction is missing');
        }
        if (!transaction.date) {
            throw new Error('The date of the transaction is missing');
        }

        return this.store.insert(transaction);
    };

    async update(props, note) {
        return this.store.update(props, note);
    }
}

export default new TransactionStore({filename: './db/transactions.json', autoload: true});