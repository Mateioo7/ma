export class Transaction {
    constructor({id, type, amount, date, madeByPartner, category, userId}) {
        this.id = id;
        this.type = type;
        this.amount = amount;
        this.date = date;
        this.madeByPartner = madeByPartner;
        this.category = category;
        this.userId = userId;
    }
}