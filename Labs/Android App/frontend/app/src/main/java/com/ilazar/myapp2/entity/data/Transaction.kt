package com.ilazar.myapp2.entity.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "transactions")
data class Transaction(
    @PrimaryKey @ColumnInfo(name = "_id") val _id: String,
    @ColumnInfo(name = "type") var type: String,
    @ColumnInfo(name = "amount") var amount: Float,
    @ColumnInfo(name = "date") var date: String,
    @ColumnInfo(name = "madeByPartner") var madeByPartner: Boolean,
    @ColumnInfo(name = "category") var category: String,
    @ColumnInfo(name = "userId") var userId: String,
) {
    override fun toString(): String {
        return when (type) {
            "income" -> "+$amount EUR"
            "expense" -> "-$amount EUR"
            else -> "<->$amount EUR"
        }
    }
}
