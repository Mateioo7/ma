package com.ilazar.myapp2.entity.item

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.BounceInterpolator
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.ilazar.myapp2.core.TAG
import com.ilazar.myapp2.databinding.FragmentItemEditBinding
import com.ilazar.myapp2.entity.data.Transaction
import java.text.SimpleDateFormat
import java.util.*
import android.R

class ItemEditFragment : Fragment() {
    companion object {
        const val ITEM_ID = "ITEM_ID"
    }

    private lateinit var viewModel: ItemEditViewModel
    private var itemId: String? = null
    private var transaction: Transaction? = null

    private val databaseDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH)
    private var calendar: Calendar = Calendar.getInstance()

    private var _binding: FragmentItemEditBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Log.i(TAG, "onCreateView")
        arguments?.let {
            if (it.containsKey(ITEM_ID)) {
                itemId = it.getString(ITEM_ID).toString()
            }
        }
        _binding = FragmentItemEditBinding.inflate(inflater, container, false)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i(TAG, "onViewCreated")
        setupViewModel()
        binding.createActionButton.setOnClickListener {
            Log.v(TAG, "save item")
            val i = transaction
            if (i != null) {
                i.type = binding.typeInput.text.toString()
                i.amount = binding.amountInput.text.toString().toFloat()
                i.category = binding.categoryInput.text.toString()
                i.date = databaseDateFormat.format(calendar.time)
                i.madeByPartner = binding.madeByPartnerCheckBox.isChecked
                viewModel.saveOrUpdateItem(i)
            }
        }

        val dateSetListener =
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            }

        val timeSetListener =
            TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                calendar.set(Calendar.HOUR, hour)
                calendar.set(Calendar.MINUTE, minute)

                binding.dateInput.text = databaseDateFormat.format(calendar.time)
            }

        binding.dateInput.setOnClickListener {
            TimePickerDialog(
                requireContext(),
                timeSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true
            ).show()

            DatePickerDialog(
                requireContext(),
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        bounceAnimation(binding.createActionButton)
    }

    private fun bounceAnimation(button: FloatingActionButton) {
        ObjectAnimator.ofFloat(button, "translationY", -300f, 0f).apply {
            duration = 1000
            interpolator = BounceInterpolator()
            repeatMode = ValueAnimator.REVERSE
            start()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        Log.i(TAG, "onDestroyView")
    }

    private fun setupViewModel() {
        viewModel = ViewModelProvider(this).get(ItemEditViewModel::class.java)
        viewModel.fetching.observe(viewLifecycleOwner, { fetching ->
            Log.v(TAG, "update fetching")
            binding.progress.visibility = if (fetching) View.VISIBLE else View.GONE
        })
        viewModel.fetchingError.observe(viewLifecycleOwner, { exception ->
            if (exception != null) {
                Log.v(TAG, "update fetching error")
                val message = "Fetching exception ${exception.message}"
                val parentActivity = activity?.parent
                if (parentActivity != null) {
                    Toast.makeText(parentActivity, message, Toast.LENGTH_SHORT).show()
                }
            }
        })
        viewModel.completed.observe(viewLifecycleOwner, { completed ->
            if (completed) {
                Log.v(TAG, "completed, navigate back")
                findNavController().navigateUp()
            }
        })
        val id = itemId
        if (id == null) {
            transaction = Transaction("", "", -1f, "", false, "", "")
        } else {
            viewModel.getItemById(id).observe(viewLifecycleOwner, {
                Log.v(TAG, "update items")
                if (it != null) {
                    transaction = it

                    binding.typeInput.setText(transaction?.type)
                    binding.amountInput.setText(transaction?.amount.toString())
                    binding.categoryInput.setText(transaction?.category)
                    binding.dateInput.text = databaseDateFormat.parse(transaction?.date).toString()


                    calendar.time = databaseDateFormat.parse(transaction?.date)

                    binding.madeByPartnerCheckBox.isChecked = transaction?.madeByPartner == true
                }
            })
        }
    }
}