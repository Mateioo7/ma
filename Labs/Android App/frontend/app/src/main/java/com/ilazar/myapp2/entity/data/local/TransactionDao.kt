package com.ilazar.myapp2.entity.data.local

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ilazar.myapp2.entity.data.Transaction

@Dao
interface TransactionDao {
    @Query("SELECT * from transactions ORDER BY date ASC")
    fun getAll(): LiveData<List<Transaction>>

    @Query("SELECT * FROM transactions WHERE _id=:id ")
    fun getById(id: String): LiveData<Transaction>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(transaction: Transaction)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(transaction: Transaction)

    @Query("DELETE FROM transactions")
    suspend fun deleteAll()
}