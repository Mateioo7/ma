package com.ilazar.myapp2.entity.data.remote

import com.ilazar.myapp2.core.Api
import com.ilazar.myapp2.entity.data.Transaction
import retrofit2.http.*

object TransactionApi {
    interface TransactionService {
        @GET("/api/item")
        suspend fun find(): List<Transaction>

        @GET("/api/item/{id}")
        suspend fun read(@Path("id") itemId: String): Transaction;

        @Headers("Content-Type: application/json")
        @POST("/api/item")
        suspend fun create(@Body transaction: Transaction): Transaction

        @Headers("Content-Type: application/json")
        @PUT("/api/item/{id}")
        suspend fun update(@Path("id") itemId: String, @Body transaction: Transaction): Transaction
    }

    val service: TransactionService = Api.retrofit.create(TransactionService::class.java)
}