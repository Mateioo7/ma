package com.ilazar.myapp2.entity.data.workers

import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.ilazar.myapp2.entity.data.Transaction
import com.ilazar.myapp2.entity.data.remote.TransactionApi
import com.ilazar.myapp2.entity.items.ItemListViewModel

class UpdateWorker(context: Context, workerParams: WorkerParameters) :
    CoroutineWorker(context, workerParams) {
    private var workerParamsData = workerParams.inputData

    override suspend fun doWork(): Result {
        Log.i("PARAMS", workerParamsData.toString())

        val updatedTransaction = Transaction(
            workerParamsData.getString("id")!!,
            workerParamsData.getString("type")!!,
            workerParamsData.getString("amount")!!.toFloat(),
            workerParamsData.getString("date")!!,
            workerParamsData.getString("madeByPartner")!!.toBoolean(),
            workerParamsData.getString("category")!!,
            workerParamsData.getString("userId")!!
        )
        TransactionApi.service.update(updatedTransaction._id, updatedTransaction)

        Log.d("UpdateWorker", "transaction updated!")
        return Result.success()
    }
}

