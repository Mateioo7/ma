package com.ilazar.myapp2.entity.data

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.work.*
import com.ilazar.myapp2.core.Result
import com.ilazar.myapp2.core.TAG
import com.ilazar.myapp2.entity.data.local.TransactionDao
import com.ilazar.myapp2.entity.data.remote.TransactionApi
import com.ilazar.myapp2.entity.data.workers.CreateWorker
import com.ilazar.myapp2.entity.data.workers.UpdateWorker
import com.ilazar.myapp2.entity.items.ItemListViewModel

class TransactionRepository(private val transactionDao: TransactionDao) : AppCompatActivity() {
    val transactions = transactionDao.getAll()

    suspend fun fetchFromServer(): Result<Boolean> {
        return try {
            val items = TransactionApi.service.find()
            for (item in items) {
                transactionDao.insert(item)
            }
            Result.Success(true)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    private fun getAll(): LiveData<List<Transaction>> {
        return transactionDao.getAll();
    }

    fun getById(itemId: String): LiveData<Transaction> {
        return transactionDao.getById(itemId)
    }

    suspend fun save(transaction: Transaction): Result<Transaction> {
        return try {
            val createdTransaction = TransactionApi.service.create(transaction)
            transactionDao.insert(createdTransaction)
            Result.Success(createdTransaction)
        } catch (e: Exception) {
            startBackgroundTask(transaction, "create")
            Result.Error(e)
        }
    }

//    fun saveAll() {
//        for (t in getAll().value!!) {
//            TransactionApi.TRANSACTION_SERVICE.update(t._id, t)
//        }
////        val createdTransaction = TransactionApi.TRANSACTION_SERVICE.create(transaction)
////        transactionDao.insert(createdTransaction)
//
////        getAll().value?.let {
////            for (transaction in getAll()) {
////                transactionDao.insert(transaction);
////            }
////        }
//    }

    suspend fun update(transaction: Transaction): Result<Transaction> {
        return try {
            val updatedTransaction =
                TransactionApi.service.update(transaction._id, transaction)
            transactionDao.update(updatedTransaction)
            Result.Success(updatedTransaction)
        } catch (e: Exception) {
            startBackgroundTask(transaction, "update")
            Result.Error(e)
        }
    }

    //    todo: maybe do the same for save??
    private fun startBackgroundTask(transaction: Transaction, jobType: String) {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val inputData = Data.Builder()
            .putString("id", transaction._id)
            .putString("type", transaction.type)
            .putString("amount", transaction.amount.toString())
            .putString("date", transaction.date)
            .putString("madeByPartner", transaction.madeByPartner.toString())
            .putString("category", transaction.category)
            .putString("userId", transaction.userId)
            .build()

        var worker = OneTimeWorkRequest.Builder(UpdateWorker::class.java)
            .setConstraints(constraints)
            .setInputData(inputData)
            .build()

        if (jobType == "create") {
            worker = OneTimeWorkRequest.Builder(CreateWorker::class.java)
                .setConstraints(constraints)
                .setInputData(inputData)
                .build()
        }

        WorkManager.getInstance(this).apply {
            enqueue(worker)
        }
    }

    private fun refreshItemList() {
        val itemListViewModel = ViewModelProvider(this).get(ItemListViewModel::class.java)
        itemListViewModel.refresh()
    }

    suspend fun deleteAll() {
        transactionDao.deleteAll()
    }
}