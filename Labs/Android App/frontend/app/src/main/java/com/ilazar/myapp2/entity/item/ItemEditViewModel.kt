package com.ilazar.myapp2.entity.item

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.ilazar.myapp2.core.Result
import com.ilazar.myapp2.core.TAG
import com.ilazar.myapp2.entity.data.Transaction
import com.ilazar.myapp2.entity.data.TransactionRepository
import com.ilazar.myapp2.entity.data.local.TransactionDatabase
import kotlinx.coroutines.launch

class ItemEditViewModel(application: Application) : AndroidViewModel(application) {
    private val mutableFetching = MutableLiveData<Boolean>().apply { value = false }
    private val mutableCompleted = MutableLiveData<Boolean>().apply { value = false }
    private val mutableException = MutableLiveData<Exception>().apply { value = null }

    val fetching: LiveData<Boolean> = mutableFetching
    val fetchingError: LiveData<Exception> = mutableException
    val completed: LiveData<Boolean> = mutableCompleted

    private val transactionRepository: TransactionRepository

    init {
        val itemDao = TransactionDatabase.getDatabase(application, viewModelScope).itemDao()
        transactionRepository = TransactionRepository(itemDao)
    }

    fun getItemById(itemId: String): LiveData<Transaction> {
        Log.v(TAG, "getItemById...")
        return transactionRepository.getById(itemId)
    }

    fun saveOrUpdateItem(transaction: Transaction) {
        viewModelScope.launch {
            Log.v(TAG, "saveOrUpdateItem...");
            mutableFetching.value = true
            mutableException.value = null

            val result: Result<Transaction> = if (transaction._id.isNotEmpty()) {
                transactionRepository.update(transaction)
            } else {
                transactionRepository.save(transaction)
            }
            when (result) {
                is Result.Success -> {
                    Log.d(TAG, "saveOrUpdateItem succeeded");
                }
                is Result.Error -> {
                    Log.w(TAG, "saveOrUpdateItem failed", result.exception);
                    mutableException.value = result.exception
                }
            }

            mutableCompleted.value = true
            mutableFetching.value = false
        }
    }
}