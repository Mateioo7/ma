package com.ilazar.myapp2.entity.data.workers

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.ilazar.myapp2.entity.data.Transaction
import com.ilazar.myapp2.entity.data.remote.TransactionApi

class CreateWorker(context: Context, workerParams: WorkerParameters) :
    CoroutineWorker(context, workerParams) {
    private var workerParamsData = workerParams.inputData

    override suspend fun doWork(): Result {
        Log.i("PARAMS", workerParamsData.toString())

        val newTransaction = Transaction(
            workerParamsData.getString("id")!!,
            workerParamsData.getString("type")!!,
            workerParamsData.getString("amount")!!.toFloat(),
            workerParamsData.getString("date")!!,
            workerParamsData.getString("madeByPartner")!!.toBoolean(),
            workerParamsData.getString("category")!!,
            workerParamsData.getString("userId")!!
        )
        TransactionApi.service.create(newTransaction)

        Log.d("CreateWorker", "transaction created!")
        return Result.success()
    }
}

